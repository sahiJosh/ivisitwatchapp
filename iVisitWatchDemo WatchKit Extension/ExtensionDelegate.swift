//
//  ExtensionDelegate.swift
//  iVisitWatchDemo WatchKit Extension
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class ExtensionDelegate: NSObject, WKExtensionDelegate {
    var becomeActive = false

    func applicationDidFinishLaunching() {
        // Perform any final initialization of your application.
    }

    func applicationDidBecomeActive() {
        becomeActive = true
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillResignActive() {
        becomeActive = false
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    func handleActionWithIdentifier(identifier: String?, forRemoteNotification remoteNotification: [NSObject : AnyObject]) {
        
        if identifier == "phoneme" {
            if let telURL=NSURL(string:"tel:9843056230") {
                let wkExtension=WKExtension.sharedExtension()
                wkExtension.openSystemURL(telURL)
            }
        }

        print(remoteNotification)
    }

}
