//
//  LineGraphCell.swift
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/28/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class LineGraphCell: NSObject {
    @IBOutlet var lblTitle: WKInterfaceLabel!
    @IBOutlet var imageGraph: WKInterfaceImage!
    @IBOutlet var groupActivity: WKInterfaceGroup!

}
