//
//  CustomerListCell.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/8/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class CustomerListCell: NSObject {
    @IBOutlet var lblDeptCode: WKInterfaceLabel!
    @IBOutlet var lblCustomerName: WKInterfaceLabel!
    @IBOutlet var lblCustomerID: WKInterfaceLabel!
    @IBOutlet var lblTurnover: WKInterfaceLabel!
    
}
