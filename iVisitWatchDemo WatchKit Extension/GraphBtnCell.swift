//
//  GraphBtnCell.swift
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/28/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

enum reportValues{
    case tappedDaily
    case tappedMonthly
    case tappedAnnually
}

protocol GraphBtnCellDelegate{
    func reportActionTapped(reportValue:reportValues)
}


class GraphBtnCell: NSObject {
    @IBOutlet var offerIndicator: WKInterfaceImage!
    @IBOutlet var orderIndicatore: WKInterfaceImage!
    @IBOutlet var btnDaily: WKInterfaceButton!
    @IBOutlet var btnMonthly: WKInterfaceButton!
    @IBOutlet var btnAnnually: WKInterfaceButton!
    @IBOutlet var lblUnit: WKInterfaceLabel!


    var delegate:GraphBtnCellDelegate?
    
    @IBAction func dailyTapped() {
        if let delegate = self.delegate{
            delegate.reportActionTapped(reportValues.tappedDaily)
        }
    }
    
    @IBAction func monthlyTapped() {
        if let delegate = self.delegate{
            delegate.reportActionTapped(reportValues.tappedMonthly)
        }

    }

    @IBAction func annuallyTapped() {
        if let delegate = self.delegate{
            delegate.reportActionTapped(reportValues.tappedAnnually)
        }

    }

}
