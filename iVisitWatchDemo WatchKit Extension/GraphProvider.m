//
//  GraphProvider.m
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/26/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

#import "GraphProvider.h"
#import "NKWatchChart/NKWatchChart.h"


@implementation GraphProvider

+ (UIImage *) getGraphImageForxAxisdata:(NSDictionary *)dictionary onImageFrame:(CGRect) frame withValue:(int) timeValue{
    
    NKLineChart *chart = [[NKLineChart alloc] initWithFrame:frame];
    chart.yLabelFormat = @"%1.0f";
    [chart setXLabels:[dictionary objectForKey:@"xLabel"]];
    chart.showCoordinateAxis = YES;
    
    //Use yFixedValueMax and yFixedValueMin to Fix the Max and Min Y Value
    //Only if you needed
    
    
    NSNumber *maxOrder=[[dictionary objectForKey:@"orderData"] valueForKeyPath:@"@max.doubleValue"];
    NSNumber *maxOffer=[[dictionary objectForKey:@"offerData"] valueForKeyPath:@"@max.doubleValue"];
    
    NSNumber *maxYValue;
    if (maxOrder.integerValue > maxOffer.integerValue) {
        maxYValue = maxOrder;
    }else{
        maxYValue = maxOffer;
    }
    
    chart.yFixedValueMax = 10;//maxYValue.integerValue + 100;
    chart.yFixedValueMin = 0.0;
    
    [chart setYLabels:[dictionary objectForKey:@"yLabel"]];
    chart.yLabelFont = [UIFont systemFontOfSize:13.f];
    
    if (timeValue == 0){
        chart.xLabelFont = [UIFont systemFontOfSize:13.f];
    }else if (timeValue == 1){
        chart.xLabelFont = [UIFont systemFontOfSize:13.f];
        
    }else if (timeValue == 2){
        chart.xLabelFont = [UIFont systemFontOfSize:13.f];
    }
    
    //    chart.xLabelWidth = 10.f;
    chart.yLabelColor = [UIColor whiteColor];
    chart.xLabelColor = [UIColor whiteColor];
    
    chart.axisColor = [UIColor whiteColor];
    chart.axisWidth = 1.f;
    
    //    chart.xUnit = @"Sales";
    //    chart.yUnit = @"Target";
    
    // Line Chart #1
    NSArray * data01Array = [dictionary objectForKey:@"orderData"];
    NKLineChartData *data01 = [NKLineChartData new];
    data01.color = NKYellow;
    data01.alpha = 1.0f;
    data01.itemCount = data01Array.count;
    data01.inflexionPointStyle = NKLineChartPointStyleCircle;
    
    data01.getData = ^(NSUInteger index) {
        CGFloat yValue = [data01Array[index] floatValue];
        return [NKLineChartDataItem dataItemWithY:yValue];
    };
    
    // Line Chart #2
//    NSArray * data02Array = [dictionary objectForKey:@"offerData"];
//    NKLineChartData *data02 = [NKLineChartData new];
//    data02.color = NKTwitterColor;
//    data02.alpha = 1.0f;
//    data02.itemCount = data02Array.count;
//    data02.inflexionPointStyle = NKLineChartPointStyleCircle;
//    data02.getData = ^(NSUInteger index) {
//        CGFloat yValue = [data02Array[index] floatValue];
//        return [NKLineChartDataItem dataItemWithY:yValue];
//    };
    
    chart.chartData = @[data01];
    UIImage *image = [chart drawImage];
    
    return image;
}

+ (UIImage *) getBarGraphImagewithFrame:(CGRect) frame{
    NKBarChart *chart = [[NKBarChart alloc] initWithFrame:frame];
    chart.yLabelFormatter = ^(CGFloat yValue){
        CGFloat yValueParsed = yValue;
        NSString * labelText = [NSString stringWithFormat:@"%0.f",yValueParsed];
        return labelText;
    };

    chart.labelTextColor = [UIColor whiteColor];
//    chart.labelFont = [UIFont systemFontOfSize:13.f];
    chart.barWidth = 10;
    chart.labelMarginTop = 5.0;
    chart.yMaxValue = 50;
    chart.showChartBorder = YES;
    [chart setXLabels:@[@"Ave",@"Bul",@"Lab", @"Ws"]];
    [chart setBarBackgroundColor:[UIColor blackColor]];
    [chart setYValues:@[@10,@40,@30,@15]];
    [chart setStrokeColors:@[NKBlueColor,NKBlueColor,NKBlueColor, NKBlueColor]];

    
    return [chart drawImage];
}


@end
