//
//  RadialDetailCell.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/5/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class RadialDetailCell: NSObject {
    @IBOutlet var lblProductGroup: WKInterfaceLabel!
    @IBOutlet var lblCYValue: WKInterfaceLabel!
    @IBOutlet var lblPYValue: WKInterfaceLabel!

}
