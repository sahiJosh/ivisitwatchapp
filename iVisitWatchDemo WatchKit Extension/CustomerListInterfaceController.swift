//
//  CustomerListInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/8/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class CustomerListInterfaceController: WKInterfaceController {
    @IBOutlet var tableView: WKInterfaceTable!
    @IBOutlet var groupUIContainer: WKInterfaceGroup!
    @IBOutlet var groupActivityIndicator: WKInterfaceGroup!
    @IBOutlet var imageActivityIndicator: WKInterfaceImage!

    var dataListArr:NSArray = []
    

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.groupUIContainer.setHidden(true)
        self.imageActivityIndicator.setImageNamed("spinner")
        self.imageActivityIndicator.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)

        loadData()
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    private func loadData(){
        let urlPath: String = "http://ipad.pfconcept.com/iVisit.V3/Notice/debtors"
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "POST"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let dayData:AnyObject = (topNode.valueForKey("DwSalesRepDebtor"))!
                    let dayDataArray = dayData as! NSArray
                    self.dataListArr = dayDataArray
                }
            }
            
            dispatch_after(0, dispatch_get_main_queue(), {
                self.groupUIContainer.setHidden(false)
                self.groupActivityIndicator.setHidden(true)
                self.imageActivityIndicator.stopAnimating()
                
                self.setTableView()
            })
        })
        
        task.resume()
    }
    
    
    private func setTableView(){
        self.tableView.setNumberOfRows(dataListArr.count, withRowType:"customerListCell")
        
        for var i = 0; i < self.tableView.numberOfRows; i++ {
            let row:AnyObject? = self.tableView.rowControllerAtIndex(i)
            let customerList = dataListArr[i]
            
            if row is CustomerListCell{
                let importantRow = row as! CustomerListCell
                importantRow.lblDeptCode.setText(customerList["DepartmentCode"] as? String)
                importantRow.lblCustomerID.setText(customerList["debtorno"] as? String)
                importantRow.lblCustomerName.setText(customerList["debtorname"] as? String)
                importantRow.lblTurnover.setText("€ \(customerList["iTargetDecliningCustomerGeneralValue"] as! String)")
            }
        }
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        let customerList = dataListArr[rowIndex]
        pushControllerWithName("RadialGraphInterface", context: customerList)
    }

}
