//
//  BarGraphInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/4/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class BarGraphInterfaceController: WKInterfaceController {
    @IBOutlet var imageGraph: WKInterfaceImage!
    @IBOutlet var iconLogo: WKInterfaceImage!
    
    private func setBarGraphView(){
        let frame:CGRect = CGRectMake(0, 0, self.contentFrame.size.width, self.contentFrame.size.height)

        let image:UIImage = GraphProvider.getBarGraphImagewithFrame(frame)
        imageGraph.setImage(image)
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    //    iconLogo.setImageNamed("ivisit-logo.png")
        setBarGraphView()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
