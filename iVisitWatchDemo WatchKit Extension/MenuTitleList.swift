//
//  MenuTitleList.swift
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/27/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import UIKit

class MenuTitleList: NSObject {
    @IBOutlet var lblTitle: WKInterfaceLabel!
    @IBOutlet var imageIcon: WKInterfaceImage!
    @IBOutlet var imageTest: WKInterfaceImage!

}
