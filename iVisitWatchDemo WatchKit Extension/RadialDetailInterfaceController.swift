//
//  RadialDetailInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/5/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class RadialDetailInterfaceController: WKInterfaceController {
    @IBOutlet var iconLogo: WKInterfaceImage!
    @IBOutlet var lblTitle: WKInterfaceLabel!
    @IBOutlet var tableView: WKInterfaceTable!
    @IBOutlet var groupUIContainer: WKInterfaceGroup!
    @IBOutlet var groupActivityIndicator: WKInterfaceGroup!
    @IBOutlet var imageActivityIndicator: WKInterfaceImage!

    var dataCustomerDict = [:]
    var dataListArr:NSArray = []
    var dataListProductGroupArr:[String] = []
    var dataListGroupArr:[ProductGroup] = []
    

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        let customerDict = context as? Dictionary<String, AnyObject>
        dataCustomerDict = customerDict!
        
        lblTitle.setText(dataCustomerDict["debtorname"] as? String)
        
        self.groupUIContainer.setHidden(true)
        self.imageActivityIndicator.setImageNamed("spinner")
        self.imageActivityIndicator.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)

        loadData()
    }

    override func willActivate() {
        super.willActivate()
        iconLogo.setImageNamed("ivisit-logo.png")
    }
    
    private func loadData(){
        let urlPath: String = "http://ipad.pfconcept.com/iVisit.V3/Notice/getDebtorProductGroup"
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let params = ["UserId":dataCustomerDict["debtorno"] as! String , "DepartmentCode":dataCustomerDict["DepartmentCode"] as! String] as Dictionary<String, String>
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "POST"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let worldData:NSArray = topNode.valueForKey("CyPyData")! as! NSArray
                    self.dataListArr = worldData
                    
                    for dataDict in self.dataListArr{
                        self.dataListProductGroupArr.append(dataDict["PRODUCT_GROUP"] as! String)
                    }
                    
                    print(self.dataListArr)
                    
                    let uniqueListArr = Array(Set(self.dataListProductGroupArr))
                    for productGroup in uniqueListArr{
                        let predicate = NSPredicate(format:"PRODUCT_GROUP == %@", productGroup)
                        let filteredArray = self.dataListArr.filteredArrayUsingPredicate(predicate)
                        
                        let productGroupObject:ProductGroup = ProductGroup()
                        
                        for dict in filteredArray{
                            if dict["TurnoverType"] as! String == "YTDPYTurnover"{
                                
                                productGroupObject.productGroupName = dict["PRODUCT_GROUP"] as? String
                                productGroupObject.turnOverPY = dict["Turnover"] as? String
                                productGroupObject.debtorNumber = dict["debtor_no"] as? String
                                
                            }else if dict["TurnoverType"] as! String == "YTDCYTurnover"{
                                
                                productGroupObject.productGroupName = dict["PRODUCT_GROUP"] as? String
                                productGroupObject.turnOverCY = dict["Turnover"] as? String
                                productGroupObject.debtorNumber = dict["debtor_no"] as? String
                            }
                        }
                        
                        self.dataListGroupArr.append(productGroupObject)
                        
                        dispatch_after(0, dispatch_get_main_queue(), {
                            self.groupUIContainer.setHidden(false)
                            self.groupActivityIndicator.setHidden(true)
                            self.imageActivityIndicator.stopAnimating()

                            self.setupTable()
                        })
                    }
                }
            }
            
        })
        task.resume()
    }

    
    private func setupTable(){
        self.tableView.setNumberOfRows(dataListGroupArr.count, withRowType:"radialDetailCell")
        
        for var i = 0; i < self.tableView.numberOfRows; i++ {
            let row:AnyObject? = self.tableView.rowControllerAtIndex(i)
            let productGroup = dataListGroupArr[i]
            let cyValue = productGroup.turnOverCY
            let pyValue = productGroup.turnOverPY

            if row is RadialDetailCell{
                let importantRow = row as! RadialDetailCell
                importantRow.lblCYValue.setTextColor(UIColor.greenColor())
                importantRow.lblPYValue.setTextColor(UIColor.greenColor())
                

                if cyValue < pyValue{
                    importantRow.lblCYValue.setTextColor(UIColor.redColor())
                    importantRow.lblPYValue.setTextColor(UIColor.redColor())
                }

                importantRow.lblProductGroup.setText(productGroup.productGroupName)
                importantRow.lblCYValue.setText("CY ")
                importantRow.lblPYValue.setText("PY ")

                if let cyValue = cyValue{
                    importantRow.lblCYValue.setText("CY €\(cyValue)")
                }
                
                if let pyValue = pyValue{
                    importantRow.lblPYValue.setText("PY €\(pyValue)")
                }
            }
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
