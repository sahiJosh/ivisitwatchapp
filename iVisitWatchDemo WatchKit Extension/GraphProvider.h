//
//  GraphProvider.h
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/26/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface GraphProvider : NSObject
+ (UIImage *) getGraphImageForxAxisdata:(NSDictionary *)dictionary onImageFrame:(CGRect) frame withValue:(int) timeValue;
+ (UIImage *) getBarGraphImagewithFrame:(CGRect) frame;

@end
