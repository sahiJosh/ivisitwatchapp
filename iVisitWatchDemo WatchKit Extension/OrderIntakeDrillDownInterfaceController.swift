//
//  OrderIntakeDrillDownInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 12/23/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class OrderIntakeDrillDownInterfaceController: WKInterfaceController {
    @IBOutlet var tableView: WKInterfaceTable!
    @IBOutlet var iconLogo: WKInterfaceImage!
//    @IBOutlet var lblTimeRemaining: WKInterfaceLabel!
    @IBOutlet var lblAchievedPercent: WKInterfaceLabel!
    @IBOutlet var groupUIContainer: WKInterfaceGroup!
    @IBOutlet var groupActivityIndicator: WKInterfaceGroup!
    @IBOutlet var imageActivityIndicator: WKInterfaceImage!

    var dataTargetArr:NSArray = []
    let startTime = "9:00"
    let targetTime = "17:00"
    var percentAchieved = ""
    var cancelAlert = false

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        percentAchieved = context as! String
    }

    override func willActivate() {
        super.willActivate()
        iconLogo.setImageNamed("ivisit-logo.png")
        

        NSNotificationCenter.defaultCenter().addObserver(self, selector: "eneteredBackground:", name: NSExtensionHostDidEnterBackgroundNotification, object: nil)
        
        if !cancelAlert{
            self.groupUIContainer.setHidden(true)
            self.groupActivityIndicator.setHidden(false)
            self.imageActivityIndicator.setImageNamed("spinner")
            self.imageActivityIndicator.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)

            loadData()
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    func eneteredBackground(notification: NSNotification){
        cancelAlert = false
    }

    private func loadData(){
        let urlPath: String = OrderIntakeDetail_RestCall
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let targetData = topNode.valueForKey("data")!.valueForKey("dept")! as! NSArray
                    self.dataTargetArr = targetData
                    
                }
                
                dispatch_after(0, dispatch_get_main_queue(), {
                    self.tableView.setHidden(false)
                    self.setTableView()
                })

            }else{
                let cancelAction = WKAlertAction(title: "Ok", style: .Default) { () -> Void in
                    self.cancelAlert = true
                }
                
                self.presentAlertControllerWithTitle("Error", message: "Error connecting to server.", preferredStyle: .Alert, actions: [cancelAction])
                dispatch_after(0, dispatch_get_main_queue(), {
                    self.tableView.setHidden(true)
                    self.lblAchievedPercent.setText("")
                })

            }
            
            
            dispatch_after(0, dispatch_get_main_queue(), {
                self.groupUIContainer.setHidden(false)
                self.groupActivityIndicator.setHidden(true)
                self.imageActivityIndicator.stopAnimating()
            })
        })
        
        task.resume()
    }
    
    func makePhoneCall(requiredObject:String){
        if let telURL = NSURL(string:requiredObject) {
            let wkExtension = WKExtension.sharedExtension()
            wkExtension.openSystemURL(telURL)
        }
    }
    
    private func calculateRemainingTimeToAchieveTarget(){
        lblAchievedPercent.setText("\(percentAchieved)% ACHIEVED")
//        lblTimeRemaining.setText("REMAINING : 0:00")
        
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let targetTimeValue: NSDate = dateFormatter.dateFromString(targetTime)!
        let startTimeValue: NSDate = dateFormatter.dateFromString(startTime)!
        
        let now = NSDate()
        let nowTimeValue:NSDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(now))!
        
        if nowTimeValue.compare(startTimeValue) == NSComparisonResult.OrderedDescending && nowTimeValue.compare(targetTimeValue) == NSComparisonResult.OrderedAscending
        {
            dateFormatter.timeZone = NSTimeZone.systemTimeZone()
            let gregorianCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let flags: NSCalendarUnit = [.Hour, .Minute]
            
            let components = gregorianCalendar.components(flags, fromDate: nowTimeValue, toDate: targetTimeValue, options: NSCalendarOptions(rawValue: 0))
            
//            lblTimeRemaining.setText("REMAINING : \(components.hour):\(components.minute)")
        }
    }
    
    private func setTableView(){
        calculateRemainingTimeToAchieveTarget()
        self.tableView.setNumberOfRows(dataTargetArr.count, withRowType:"targetDrillDownCell")
        
        for var i = 0; i < self.tableView.numberOfRows; i++ {
            let row:AnyObject? = self.tableView.rowControllerAtIndex(i)
            let drillDownTarget = dataTargetArr[i]
            
            if row is TargetDrillDownCell{
                let importantRow = row as! TargetDrillDownCell
                let achievedValue = drillDownTarget["CY_SALESAMOUNT"]?!.doubleValue
                let targetValue = drillDownTarget["PY_SALESAMOUNT"]?!.doubleValue
                
                var percentValue:Double = 0
                
                if targetValue != 0{
                    percentValue = 100 * achievedValue!/targetValue!
                }else if targetValue == 0{
                    percentValue = 100
                }
                
                if achievedValue == 0{
                    percentValue = 0
                }
                
                var requiredValue = Int(percentValue)
                
                if requiredValue >= 100{
                    requiredValue = 101
                }
                
                let formatter = NSNumberFormatter()
                formatter.decimalSeparator = "."
                formatter.maximumFractionDigits = 0
                formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
                let requiredPercent = formatter.stringFromNumber(percentValue)
                
                importantRow.lblDeptCode.setText(drillDownTarget["COUNTRY_MANAGER"]! as? String)
                importantRow.lblPercentageAchieved.setText("\(requiredPercent!) %")

                if requiredValue <= 0{
                    importantRow.groupProgressBar.setBackgroundImageNamed("bar0")

                }else{
                    importantRow.groupProgressBar.setBackgroundImageNamed("bar")
                    importantRow.groupProgressBar.startAnimatingWithImagesInRange(NSMakeRange(0, requiredValue), duration: 1, repeatCount: 1)
                }
                
            }
        }
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        let dataTarget = dataTargetArr[rowIndex] as! NSDictionary
        
        let callAction = WKAlertAction(title: "Call", style: .Default) { () -> Void in
            let phoneNum = String(format: "tel:%@", String(dataTarget["PHONE_NO"]!))
            self.performSelector(Selector("makePhoneCall:"), withObject: phoneNum, afterDelay: 1)
        }
        
        let cancelAction = WKAlertAction(title: "Cancel", style: .Default) { () -> Void in
            
        }
        
        presentAlertControllerWithTitle("", message: "Do you want to make a call to \(dataTarget["MANAGER"]!)?", preferredStyle: .SideBySideButtonsAlert, actions: [cancelAction, callAction])
    }

}
