//
//  SatisfactionRateInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright (c) 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation

class SatisfactionRateInterfaceController: WKInterfaceController, GraphBtnCellDelegate {
    @IBOutlet var tableView: WKInterfaceTable!
    @IBOutlet var groupGraph: WKInterfaceGroup!
    @IBOutlet var groupActivity: WKInterfaceGroup!
    @IBOutlet var activityImage: WKInterfaceImage!
    @IBOutlet var iconLogo: WKInterfaceImage!

    
    var linegraphCell:LineGraphCell?
    var btngraphCell:GraphBtnCell?
    var dataDayArr:NSArray = []
    var dataMonthArr:NSArray = []
    var dataAnnualArr:NSArray = []
    
    
    //Mark: Private Apis
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        self.groupGraph.setHidden(true)
        self.activityImage.setImageNamed("spinner")
        self.activityImage.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)

        loadData()
    }

    override func willActivate() {
        super.willActivate()
        iconLogo.setImageNamed("ivisit-logo.png")
    }

    override func didDeactivate() {
        super.didDeactivate()
    }
    
    
    private func loadData(){
        let urlPath: String = "http://ipad.pfconcept.com/iVisit.V3/Notice/satisfactorytrend"
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let dayData:AnyObject = topNode.valueForKey("data")!.valueForKey("day")!
                    let dayDataArray = dayData as! NSArray
                    self.dataDayArr = dayDataArray.reverseObjectEnumerator().allObjects
                    
                    let  monthData:AnyObject = topNode.valueForKey("data")!.valueForKey("Month")!
                    let monthDataArray = monthData as! NSArray
                    self.dataMonthArr = monthDataArray.reverseObjectEnumerator().allObjects
                    
                    let  annuallData:AnyObject = topNode.valueForKey("data")!.valueForKey("Annual")!
                    let annuallDataArray = annuallData as! NSArray
                    self.dataAnnualArr = annuallDataArray.reverseObjectEnumerator().allObjects
                    
                }
            }
            
            dispatch_after(0, dispatch_get_main_queue(), {
                self.groupGraph.setHidden(false)
                self.groupActivity.setHidden(true)
                self.activityImage.stopAnimating()
                
                self.setTableView()
            })
        })
        
        task.resume()
    }
    
    private func showMonthlyReport(){
        self.btngraphCell?.btnAnnually.setBackgroundColor(UIColor.clearColor())
        self.btngraphCell?.btnMonthly.setBackgroundColor(UIColor(red: 28.0/255, green: 36.0/255, blue: 60.0/255, alpha: 1.0))
        
        let frame:CGRect = CGRectMake(0, 0, self.contentFrame.size.width, self.contentFrame.size.height)
        var xlabelsArr:[String] = []
        let ylabelsArr = ["0", "2", "4", "6", "8", "10"]
        
        var yAxisDataArrAverageRate:[String] = []
        for dict in dataMonthArr{
            xlabelsArr.append(dict["date"] as! String)
            let yOrderValue = dict["satisfactionValue"]
            if let yOrderValue = yOrderValue as? Double{
                yAxisDataArrAverageRate.append(String(format: "%f", yOrderValue))
            }
        }
        
        let dict = ["xLabel": xlabelsArr, "yLabel": ylabelsArr, "orderData" : yAxisDataArrAverageRate];
        
        let image:UIImage = GraphProvider.getGraphImageForxAxisdata(dict as [NSObject : AnyObject], onImageFrame: frame, withValue:0)
        self.linegraphCell?.imageGraph.setImage(image)
    }
    
    private func showAnnuallReport(){
        self.btngraphCell?.btnMonthly.setBackgroundColor(UIColor.clearColor())
        self.btngraphCell?.btnAnnually.setBackgroundColor(UIColor(red: 28.0/255, green: 36.0/255, blue: 60.0/255, alpha: 1.0))
        
        let frame:CGRect = CGRectMake(0, 0, self.contentFrame.size.width, self.contentFrame.size.height)
        
        var xlabelsArr:[String] = []
        let ylabelsArr = ["0", "2", "4", "6", "8", "10"]
        
        var yAxisDataArrAverageRate:[String] = []
        for dict in dataAnnualArr{
            xlabelsArr.append(dict["date"] as! String)
            let yOrderValue = dict["satisfactionValue"]
            if let yOrderValue = yOrderValue as? Double{
                yAxisDataArrAverageRate.append(String(format: "%f", yOrderValue))
            }
            
        }
        
        let dict = ["xLabel": xlabelsArr, "yLabel": ylabelsArr, "orderData" : yAxisDataArrAverageRate];
        
        let image:UIImage = GraphProvider.getGraphImageForxAxisdata(dict as [NSObject : AnyObject], onImageFrame: frame, withValue:0)
        self.linegraphCell?.imageGraph.setImage(image)
    }
    
    
    private func setTableView(){
        self.tableView.setRowTypes(["LineGraphCell", "GraphBtnCell"])
        for var i = 0; i < self.tableView.numberOfRows; i++ {
            let row:AnyObject? = self.tableView.rowControllerAtIndex(i)
            if row is LineGraphCell{
                let linegraphCell = row as! LineGraphCell
                self.linegraphCell = linegraphCell
                
                dispatch_after(1, dispatch_get_main_queue(), {
                    self.showMonthlyReport()
                })
                
            }else{
                let graphBtnCell = row as! GraphBtnCell
                self.btngraphCell = graphBtnCell
                graphBtnCell.delegate = self
            }
        }
    }
    
    //Mark: Delegate Methods

    func reportActionTapped(reportValue:reportValues){
        switch reportValue {
        case .tappedDaily: break
//            showDailyReport()
        case .tappedMonthly:
            showMonthlyReport()
        case .tappedAnnually:
            showAnnuallReport()
        }
    }


}
