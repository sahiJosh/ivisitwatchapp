//
//  TargetDrillDownCell.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 12/14/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class TargetDrillDownCell: NSObject {
    @IBOutlet var groupProgressBar: WKInterfaceGroup!
    @IBOutlet var lblDeptCode: WKInterfaceLabel!
    @IBOutlet var lblPercentageAchieved: WKInterfaceLabel!
}
