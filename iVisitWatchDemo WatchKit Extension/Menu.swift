//
//  Menu.swift
//  WatchDemoProject
//
//  Created by Sahi Joshi on 9/27/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class Menu: NSObject {
    var menuTitle:String
    var menuImageIcon:String?
    
    init(dataDictionary:Dictionary<String,String>) {
        self.menuTitle = dataDictionary["eventTitle"]!
        self.menuImageIcon = dataDictionary["eventImageName"]!
    }
    
    class func menuList() -> [Menu]{
        var array = [Menu]()
        let dataPath = NSBundle.mainBundle().pathForResource("menus", ofType: "plist")

        let data = NSArray(contentsOfFile: dataPath!)
        
        for e in data as! [Dictionary<String, String>] {
            let event = Menu(dataDictionary: e)
            array.append(event)
        }
        
        return array
    }
}
