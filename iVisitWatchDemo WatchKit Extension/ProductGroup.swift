//
//  ProductGroup.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/16/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit

class ProductGroup: NSObject {
    var productGroupName:String?
    var turnOverPY:String?
    var turnOverCY:String?
    var debtorNumber:String?
}
