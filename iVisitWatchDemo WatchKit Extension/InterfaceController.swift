//
//  InterfaceController.swift
//  iVisitWatchDemo WatchKit Extension
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    @IBOutlet var tableView: WKInterfaceTable!
    @IBOutlet var iconLogo: WKInterfaceImage!

    var menusData:[Menu]?
    var selectedIndex:Int?

    //Mark: System Methods

    override func awakeWithContext(context: AnyObject?) {
        menusData = Menu.menuList()
        super.awakeWithContext(context)
    }

    override func willActivate() {
        super.willActivate()
        iconLogo.setImageNamed("ivisit-logo.png")
        setupTable();
    }

    override func didDeactivate() {
        super.didDeactivate()
    }
    
    //Mark: Delegate Methods
    
    private func setupTable(){
        self.tableView.setNumberOfRows(menusData!.count, withRowType:"MenuTitleList")
        
        for var i = 0; i < self.tableView.numberOfRows; i++ {
            let row:AnyObject? = self.tableView.rowControllerAtIndex(i)
            let menu = menusData![i]
            
            if row is MenuTitleList{
                let importantRow = row as! MenuTitleList
                importantRow.imageIcon.setImageNamed(menu.menuImageIcon!)
                importantRow.lblTitle.setText(menu.menuTitle)
                
                if selectedIndex == i{
                    importantRow.imageIcon.setImageNamed("select.png")
                }
            }
        }
    }
    
    override func table(table: WKInterfaceTable, didSelectRowAtIndex rowIndex: Int) {
        selectedIndex = rowIndex
        
        if rowIndex == 0{
            pushControllerWithName("orderIntake", context: nil)
        }else if rowIndex == 1{
//            let controllerNames:Array = ["RadialGraphInterface", "RadialGraphInterface", "RadialGraphInterface", "RadialGraphInterface"]
//            let contexts:Array = ["1", "2", "3", "4"]
//            presentControllerWithNames(controllerNames, contexts: contexts)
            pushControllerWithName("targetDaily", context: true)
        }
//        else if rowIndex == 2{
//            pushControllerWithName("SatisfactoryRate", context: true)
//        }else if rowIndex == 3{
//            pushControllerWithName("customerList", context: true)
//        }
    }
}
