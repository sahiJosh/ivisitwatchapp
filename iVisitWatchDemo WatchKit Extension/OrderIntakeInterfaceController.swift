//
//  OrderIntakeInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 12/23/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class OrderIntakeInterfaceController: WKInterfaceController {
    @IBOutlet var iconLogo: WKInterfaceImage!
    @IBOutlet var lblTitle: WKInterfaceLabel!
    @IBOutlet var groupSingleArc: WKInterfaceGroup!
    @IBOutlet var lblActualPercentage: WKInterfaceLabel!
    @IBOutlet var lblHoursRemaining: WKInterfaceLabel!
    @IBOutlet var lblActualValue: WKInterfaceLabel!
    @IBOutlet var lblTargetValue: WKInterfaceLabel!

    @IBOutlet var groupUIContainer: WKInterfaceGroup!
    @IBOutlet var groupActivityIndicator: WKInterfaceGroup!
    @IBOutlet var imageActivityIndicator: WKInterfaceImage!
    
    var dataTargetDict:NSDictionary = NSDictionary()
    var achievedPercentageValue = ""
    var targetValue:Double = 0
    var actualValue:Double = 0
    let startTime = "9:00"
    let targetTime = "17:00"
    var cancelAlert = false

    let animateDuration = 1.2

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
    }

    override func willActivate() {
        super.willActivate()
        targetValue = 0
        actualValue = 0
        iconLogo.setImageNamed("ivisit-logo.png")


        NSNotificationCenter.defaultCenter().addObserver(self, selector: "eneteredBackground:", name: NSExtensionHostDidEnterBackgroundNotification, object: nil)
        
        if !cancelAlert{
            self.groupUIContainer.setHidden(true)
            self.groupActivityIndicator.setHidden(false)
            self.imageActivityIndicator.setImageNamed("spinner")
            self.imageActivityIndicator.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)

            loadData()
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
        cancelAlert = false
        print(segueIdentifier)
        if segueIdentifier == "orderIntakeDrillDown"{
            return achievedPercentageValue
        }
        return ""
    }
    
    func eneteredBackground(notification: NSNotification){
        cancelAlert = false
    }


    private func loadData(){
        let urlPath: String = OrderIntake_RestCall
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let targetData = topNode.valueForKey("data") as! NSDictionary
                    self.actualValue = targetData["TOTAL_CY_SALESAMOUNT"]!.doubleValue
                    self.targetValue = targetData["TOTAL_PY_SALESAMOUNT"]!.doubleValue
                }
                
                dispatch_after(0, dispatch_get_main_queue(), {
                    self.displayRadialChart()
                })

            }else{
                dispatch_after(0, dispatch_get_main_queue(), {
                    let cancelAction = WKAlertAction(title: "Ok", style: .Default) { () -> Void in
                        self.cancelAlert = true
                    }
                    self.presentAlertControllerWithTitle("Error", message: "Error connecting to server.", preferredStyle: .Alert, actions: [cancelAction])
                    dispatch_after(0, dispatch_get_main_queue(), {
                        self.groupSingleArc.setBackgroundImageNamed("single0")
                        self.lblActualPercentage.setText("")
                        self.lblTargetValue.setText("")
                        self.lblActualValue.setText("")
                        self.achievedPercentageValue = ""
                    })
                })
            }
            
            
            dispatch_after(0, dispatch_get_main_queue(), {
                self.groupUIContainer.setHidden(false)
                self.groupActivityIndicator.setHidden(true)
                self.imageActivityIndicator.stopAnimating()
            })
        })
        
        task.resume()
    }
    
    func forTailingZero(temp: Double) -> String{
        let formatter = NSNumberFormatter()
        formatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.groupingSeparator = ","
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter.stringFromNumber(temp)!
    }

    
    private func displayRadialChart(){
//        lblHoursRemaining.setText("HOURS REMAINING : 0:00")
        lblHoursRemaining.setText("")
        
        var percentValue = 100 * actualValue/targetValue
        
        if targetValue == 0{
            percentValue = 100
        }
        
        if actualValue == 0{
            percentValue = 0
        }
        
        let requiredValue = Int(percentValue)
        
        let formatter = NSNumberFormatter()
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 0
        formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        let requiredPercent = formatter.stringFromNumber(percentValue)

        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        let targetTimeValue: NSDate = dateFormatter.dateFromString(targetTime)!
        let startTimeValue: NSDate = dateFormatter.dateFromString(startTime)!
        
        let now = NSDate()
        let nowTimeValue:NSDate = dateFormatter.dateFromString(dateFormatter.stringFromDate(now))!
        
        if nowTimeValue.compare(startTimeValue) == NSComparisonResult.OrderedDescending && nowTimeValue.compare(targetTimeValue) == NSComparisonResult.OrderedAscending
        {
            dateFormatter.timeZone = NSTimeZone.systemTimeZone()
            let gregorianCalendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let flags: NSCalendarUnit = [.Hour, .Minute]
            
            let components = gregorianCalendar.components(flags, fromDate: nowTimeValue, toDate: targetTimeValue, options: NSCalendarOptions(rawValue: 0))
            
            print("\(components.hour):\(components.minute)")
//            lblHoursRemaining.setText("HOURS REMAINING : \(components.hour):\(components.minute)")
            lblHoursRemaining.setText("")
        }
        
        
        
        lblActualPercentage.setText("\(requiredPercent!) %")
        achievedPercentageValue = requiredPercent!
        let targetValueString = forTailingZero(targetValue)
        let actualValueString = forTailingZero(actualValue)

        lblTargetValue.setText("€\(targetValueString)")
        lblActualValue.setText("€\(actualValueString)")
        
        if requiredValue > 0{
            groupSingleArc.setBackgroundImageNamed("single")
            groupSingleArc.startAnimatingWithImagesInRange(NSMakeRange(0, requiredValue), duration: animateDuration, repeatCount: 1)
        }
    }
}
