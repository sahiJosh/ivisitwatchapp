//
//  RadialGraphInterfaceController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import WatchKit
import Foundation


class RadialGraphInterfaceController: WKInterfaceController {
    let duration = 0.5

    @IBOutlet var bottomGroup: WKInterfaceGroup!
    @IBOutlet var titileGroup: WKInterfaceGroup!
    @IBOutlet var outerGroup: WKInterfaceGroup!
    @IBOutlet var middleGroup: WKInterfaceGroup!
    @IBOutlet var innerGroup: WKInterfaceGroup!
    @IBOutlet var lblTitleWorld: WKInterfaceLabel!
    @IBOutlet var btnActionButton: WKInterfaceButton!
    @IBOutlet var lblBulletValue: WKInterfaceLabel!
    @IBOutlet var lblAvenueValue: WKInterfaceLabel!
    @IBOutlet var lblLabelValue: WKInterfaceLabel!
    @IBOutlet var iconLogo: WKInterfaceImage!
    @IBOutlet var groupUIContainer: WKInterfaceGroup!
    @IBOutlet var groupActivityIndicator: WKInterfaceGroup!
    @IBOutlet var imageActivityIndicator: WKInterfaceImage!

    var dataListDict:NSDictionary = [:]
    var dataCustomerDict = [:]
    
    var animated = false
    var pagetitle = ""
    var arcs = 0
    var arcOuter = ""
    var arcMiddle = ""
    var arcInner = ""
    var arcOuterVal = 1.0
    var arcOuterMax = 1.0
    var arcMiddleVal = 1.0
    var arcMiddleMax = 1.0
    var arcInnerVal = 1.0
    var arcInnerMax = 1.0
    
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        let customerDict = context as? Dictionary<String, AnyObject>
        dataCustomerDict = customerDict!

        lblTitleWorld.setText(dataCustomerDict["debtorname"] as? String)
        
        self.groupUIContainer.setHidden(true)
        self.imageActivityIndicator.setImageNamed("spinner")
        self.imageActivityIndicator.startAnimatingWithImagesInRange(NSRange(location: 0, length: 42), duration: 1, repeatCount:0)
        loadData()
    }

    override func willActivate() {
        super.willActivate()
        iconLogo.setImageNamed("ivisit-logo.png")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    private func displayRadialChart(){
        let bulletTurnover = dataListDict["bulletturnover"]!.integerValue
        let avenueTurnover = dataListDict["avenueturnover"]!.integerValue
        let labelTurnover =  dataListDict["labelturnover"]!.integerValue
        
        lblBulletValue.setText("€ \(bulletTurnover)")
        lblAvenueValue.setText("€ \(avenueTurnover)")
        lblLabelValue.setText("€ \(labelTurnover)")
        
        
        var totalWorldTurnover = bulletTurnover + avenueTurnover + labelTurnover
        if totalWorldTurnover == 0{
            totalWorldTurnover = 1;
        }
        
        var pageObjects:  [AnyObject] = []
        pageObjects.append([
            "title":"Activity", "arcs":3, "arcOuter":"outer", "arcMiddle":"middle", "arcInner":"inner", "arcOuterVal":bulletTurnover, "arcOuterMax":totalWorldTurnover, "arcMiddleVal":avenueTurnover, "arcMiddleMax":totalWorldTurnover, "arcInnerVal":labelTurnover, "arcInnerMax":totalWorldTurnover, "units": ""
            ])
        
        animated = false
        
        arcs = pageObjects[0].valueForKey("arcs") as! Int
        arcOuter = pageObjects[0].valueForKey("arcOuter") as! String
        arcMiddle = pageObjects[0].valueForKey("arcMiddle") as! String
        arcInner = pageObjects[0].valueForKey("arcInner") as! String
        arcOuterVal = pageObjects[0].valueForKey("arcOuterVal") as! Double
        arcOuterMax = pageObjects[0].valueForKey("arcOuterMax") as! Double
        arcMiddleVal = pageObjects[0].valueForKey("arcMiddleVal") as! Double
        arcMiddleMax = pageObjects[0].valueForKey("arcMiddleMax") as! Double
        arcInnerVal = pageObjects[0].valueForKey("arcInnerVal") as! Double
        arcInnerMax = pageObjects[0].valueForKey("arcInnerMax") as! Double
        
        print(arcOuterVal)
        print(arcOuterMax)
        print(arcMiddleVal)
        print(arcMiddleMax)
        print(arcInnerVal)
        print(arcInnerMax)
        
        if(!animated){
            if(arcs == 3){
                outerGroup.setBackgroundImageNamed(arcOuter)
                middleGroup.setBackgroundImageNamed(arcMiddle)
                innerGroup.setBackgroundImageNamed(arcInner)
                outerGroup.startAnimatingWithImagesInRange(NSMakeRange(0, Int(arcOuterVal/arcOuterMax*100)+1), duration: duration, repeatCount: 1)
                middleGroup.startAnimatingWithImagesInRange(NSMakeRange(0, Int(arcMiddleVal/arcMiddleMax*100)+1), duration: duration, repeatCount: 1)
                innerGroup.startAnimatingWithImagesInRange(NSMakeRange(0, Int(arcInnerVal/arcInnerMax*100)+1), duration: duration, repeatCount: 1)
                animated = true
            }
        }
    }
    
    private func loadData(){
        let urlPath: String = "http://ipad.pfconcept.com/iVisit.V3/Notice/getDebtorDetail"
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let params = ["UserId":dataCustomerDict["debtorno"] as! String , "DepartmentCode":dataCustomerDict["DepartmentCode"] as! String] as Dictionary<String, String>
        
        request.HTTPBody = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "POST"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let worldData:NSDictionary = topNode.valueForKey("data")! as! NSDictionary
                    self.dataListDict = worldData
                    print(self.dataListDict)
                }
            }
            
            dispatch_after(0, dispatch_get_main_queue(), {
                self.groupUIContainer.setHidden(false)
                self.groupActivityIndicator.setHidden(true)
                self.imageActivityIndicator.stopAnimating()

                self.displayRadialChart()
            })
        })
        
        task.resume()
    }
    
    
    @IBAction func detailAction() {
        self.pushControllerWithName("radialDetail", context:dataCustomerDict)
    }

}
