//
//  BaseViewController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Josh on 1/26/16.
//  Copyright © 2016 Sahi Joshi. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setBackgroundColor(UIColor.darkGrayColor())
        SVProgressHUD.setForegroundColor(UIColor.whiteColor())
        
        let button: UIButton = UIButton(type: UIButtonType.Custom)
        button.setImage(UIImage(named: "icon-thumb"), forState: UIControlState.Normal)
        
        let negativeSpacer: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        negativeSpacer.width = 2
        
        button.frame = CGRectMake(0, 0, 35, 35)
        let menuButton = UIBarButtonItem(customView: button)
        navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        navigationController?.navigationBar.translucent = true;
        navigationController?.navigationBar.barStyle = UIBarStyle.BlackTranslucent;

        navigationItem.setLeftBarButtonItems([negativeSpacer, menuButton], animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
