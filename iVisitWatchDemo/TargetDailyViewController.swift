//
//  TargetDailyViewController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Josh on 1/27/16.
//  Copyright © 2016 Sahi Joshi. All rights reserved.
//

import UIKit
import SVProgressHUD

class TargetDailyViewController: UIViewController {
    @IBOutlet var iconLogo: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblActualPercentage: UILabel!
    @IBOutlet var lblActualValue: UILabel!
    @IBOutlet var lblTargetValue: UILabel!
    @IBOutlet weak var graphImageView: UIImageView!
    
    var targetValue:Double = 0
    var actualValue:Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.automaticallyAdjustsScrollViewInsets = false

        targetValue = 0
        actualValue = 0
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "eneteredForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)

        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(animated: Bool) {
        SVProgressHUD.dismiss()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    func eneteredForeground(notification: NSNotification){
        targetValue = 0
        actualValue = 0
        loadData()
    }

    @IBAction func goToDetailAction(sender: AnyObject) {
        let targeteDetailVC = storyboard?.instantiateViewControllerWithIdentifier("targetDailyDetail") as! TargetDailyDetailViewController
        targeteDetailVC.percentAchieved = lblActualPercentage.text!
        navigationController?.pushViewController(targeteDetailVC, animated: true)
    }
    
    
    private func loadData(){
        SVProgressHUD.showWithStatus("Loading")

        let urlPath: String = TargetDaily_RestCall
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let targetData = topNode.valueForKey("data") as! NSDictionary
                    self.actualValue = targetData["achieved"]!.doubleValue
                    self.targetValue = targetData["target"]!.doubleValue
                }
                
                dispatch_after(0, dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                    self.displayRadialChart()
                })
                
            }else{
                dispatch_after(0, dispatch_get_main_queue(), {
                    SVProgressHUD.showErrorWithStatus("Error connecting to server.")
                    self.graphImageView.image = UIImage(named: "single0")
                    self.lblActualPercentage.text = ""
                    self.lblTargetValue.text = ""
                    self.lblActualValue.text = ""
                })
            }
        })
        
        task.resume()
    }
    
    private func forTailingZero(temp: Double) -> String{
        let formatter = NSNumberFormatter()
        formatter.formatterBehavior = NSNumberFormatterBehavior.Behavior10_4
        formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        formatter.groupingSeparator = ","
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        return formatter.stringFromNumber(temp)!
    }
    
    
    private func displayRadialChart(){
        var percentValue = 100 * actualValue/targetValue
        
        if targetValue == 0{
            percentValue = 100
        }
        
        if actualValue == 0{
            percentValue = 0
        }
        
        let requiredValue = Int(percentValue)
        
        let formatter = NSNumberFormatter()
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 0
        formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        let requiredPercent = formatter.stringFromNumber(percentValue)
        
        lblActualPercentage.text = "\(requiredPercent!) %"
        let targetValueString = forTailingZero(targetValue)
        let actualValueString = forTailingZero(actualValue)
        
        lblTargetValue.text = "€\(targetValueString)"
        lblActualValue.text = "€\(actualValueString)"
        
        if requiredValue > 0{
            var imagesArr = [UIImage]()
            var lastIndex = 0
            
            for var index in 0...requiredValue{
                if index > 200{
                    index = 200
                }
                
                lastIndex = index
                imagesArr.append(UIImage(named: "single\(index)")!)
            }
            
            graphImageView.image = UIImage(named: "single\(lastIndex)")
            graphImageView.animationDuration = 1
            graphImageView.animationRepeatCount = 1
            graphImageView.animationImages = imagesArr
            graphImageView.startAnimating()
        }
    }
}
