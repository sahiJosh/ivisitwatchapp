//
//  ViewController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import UIKit

class ViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var menusData:[Menu]?
    var selectedIndex:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.contentInset = UIEdgeInsetsMake(44, 0, 0, 0)
        menusData = Menu.menuList()
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (menusData?.count)!
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! MenuCell
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let menu = menusData![indexPath.row]
        cell.lblMenuTitle.text = menu.menuTitle
        cell.imageIcon.image = UIImage(named: menu.menuImageIcon!)
        let disclosure = UIImageView(image: UIImage(named: "disclosure"))
        cell.accessoryView = disclosure
        
        if selectedIndex == indexPath.row{
            cell.imageIcon.image = UIImage(named: "select.png")
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?{
        let view = UIView()
        view.frame = CGRectMake(0, 0, tableView.frame.size.width, 1)
        view.backgroundColor = UIColor.clearColor()
        return view
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        selectedIndex = indexPath.row
        switch (indexPath.row) {
        case 0:
            let orderIntakeVC = storyboard?.instantiateViewControllerWithIdentifier("orderIntake") as! OrderIntakeViewController
            navigationController?.pushViewController(orderIntakeVC, animated: true)
            break;
        case 1:
            let targetDaily = storyboard?.instantiateViewControllerWithIdentifier("targetDaily") as! TargetDailyViewController
            navigationController?.pushViewController(targetDaily, animated: true)
            break;
            
        default:
            break;
        }
        
    }
    
}

