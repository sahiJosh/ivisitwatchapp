//
//  OrderIntakeDetailCell.swift
//  iVisitWatchDemo
//
//  Created by Sahi Josh on 1/26/16.
//  Copyright © 2016 Sahi Joshi. All rights reserved.
//

import UIKit

class OrderIntakeDetailCell: UITableViewCell {
    @IBOutlet weak var lblDeptName: UILabel!
    @IBOutlet weak var lblPercentAchieved: UILabel!
    @IBOutlet weak var graphImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
