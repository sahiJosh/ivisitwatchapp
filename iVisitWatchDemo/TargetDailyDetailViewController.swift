//
//  TargetDailyDetailViewController.swift
//  iVisitWatchDemo
//
//  Created by Sahi Josh on 1/27/16.
//  Copyright © 2016 Sahi Joshi. All rights reserved.
//

import UIKit
import SVProgressHUD

class TargetDailyDetailViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var iconLogo: UIImageView!
    @IBOutlet var lblAchievedPercent: UILabel!
    
    var percentAchieved = ""
    var dataTargetArr:NSArray = []
    var animateCellArr = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50

        lblAchievedPercent.text = "\(percentAchieved) ACHIEVED"
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "eneteredForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)

        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidDisappear(animated: Bool) {
        SVProgressHUD.dismiss()
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func eneteredForeground(notification: NSNotification){
        loadData()
    }

    private func loadData(){
        SVProgressHUD.showWithStatus("Loading")

        let urlPath: String = TargetDailyDetail_RestCall
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        print(encodedUrl)
        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as NSError).localizedDescription)")
                }
                
                if let topNode = jsonResults as? NSDictionary{
                    let targetData = topNode.valueForKey("data")!.valueForKey("dept")! as! NSArray
                    self.dataTargetArr = targetData
                    
                }
                
                dispatch_after(0, dispatch_get_main_queue(), {
                    SVProgressHUD.dismiss()
                    self.tableView.hidden = false
                    self.tableView.reloadData()
                })
                
            }else{
                dispatch_after(0, dispatch_get_main_queue(), {
                    SVProgressHUD.showErrorWithStatus("Error connecting to server.")
                    
                    self.tableView.hidden = true
                    self.lblAchievedPercent.text = ""
                })
                
            }
            
        })
        
        task.resume()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return (dataTargetArr.count)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cell") as! OrderIntakeDetailCell
        let data = dataTargetArr[indexPath.row]
        
        let achievedValue = data["acheived"]?!.doubleValue
        let targetValue = data["target"]?!.doubleValue
        
        var percentValue:Double = 0
        
        if targetValue != 0{
            percentValue = 100 * achievedValue!/targetValue!
        }else if targetValue == 0{
            percentValue = 100
        }
        
        if achievedValue == 0{
            percentValue = 0
        }
        
        var requiredValue = Int(percentValue)
        
        if requiredValue >= 100{
            requiredValue = 101
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        let formatter = NSNumberFormatter()
        formatter.decimalSeparator = "."
        formatter.maximumFractionDigits = 0
        formatter.roundingMode = NSNumberFormatterRoundingMode.RoundHalfUp
        let requiredPercent = formatter.stringFromNumber(percentValue)
        
        cell.lblDeptName.text = data["deparment"]! as? String
        cell.lblPercentAchieved.text = "\(requiredPercent!) %"
        
        if requiredValue <= 0{
            cell.graphImageView.image = UIImage(named: "bar0")
        }else{
            var imagesArr = [UIImage]()
            var lastIndex = 0
            
            for var index in 0...requiredValue{
                if index > 100{
                    index = 100
                }
                
                lastIndex = index
                imagesArr.append(UIImage(named: "bar\(index)")!)
            }
            cell.graphImageView.image = UIImage(named: "bar\(lastIndex)")
            cell.graphImageView.animationDuration = 1
            cell.graphImageView.animationRepeatCount = 1
            cell.graphImageView.animationImages = imagesArr
            
            if !animateCellArr.contains(indexPath.row){
                cell.graphImageView.startAnimating()
            }
            
            if !animateCellArr.contains(indexPath.row){
                animateCellArr.append(indexPath.row)
            }
            

        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let dataTarget = dataTargetArr[indexPath.row] as! NSDictionary
        
        let callBtn = UIAlertAction(title: "Call", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            let phoneNum = String(format: "tel://%@", String(dataTarget["phoneno"]!))
            self.performSelector(Selector("makePhoneCall:"), withObject: phoneNum, afterDelay: 1)
        })
        
        let cancelBtn = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: { (UIAlertAction) -> Void in
            
        })
        
        let alert = UIAlertController(title: "", message: "Do you want to make a call to \(dataTarget["manager"]!)?", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(callBtn)
        alert.addAction(cancelBtn)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func makePhoneCall(requiredObject:String){
        if let telURL = NSURL(string:requiredObject) {
            UIApplication.sharedApplication().openURL(telURL);
        }
    }

}
