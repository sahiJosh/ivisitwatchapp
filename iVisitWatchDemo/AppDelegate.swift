//
//  AppDelegate.swift
//  iVisitWatchDemo
//
//  Created by Sahi Joshi on 11/3/15.
//  Copyright © 2015 Sahi Joshi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func registerSettingsAndCategories() {
        let acceptAction = UIMutableUserNotificationAction()
        acceptAction.title = NSLocalizedString("Call", comment: "Call number")
        acceptAction.identifier = "phoneme"
        acceptAction.activationMode = UIUserNotificationActivationMode.Foreground
        acceptAction.authenticationRequired = false
        
        let inviteCategory = UIMutableUserNotificationCategory()
        inviteCategory.setActions([acceptAction],
            forContext: UIUserNotificationActionContext.Default)
        inviteCategory.identifier = "Callme"
        
        // Configure other actions and categories and add them to the set...
        
        let settings =  UIUserNotificationSettings(
            forTypes: [.Alert, .Badge, .Sound],
            categories: (NSSet(array: [inviteCategory])) as? Set<UIUserNotificationCategory>)
        
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    }

    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }

    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        let characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        let deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        
        let urlPath: String = NSString(format: "https://ipad.pfconcept.com/iVisit.V3/public/getdevicedet.php?deviceToken=%@&deviceId=%@", deviceTokenString, UIDevice.currentDevice().identifierForVendor!.UUIDString) as String
        let encodedUrl:String = urlPath.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
        
        NSLog("\(deviceTokenString)")
        NSLog("\(encodedUrl)")

        
        let request = NSMutableURLRequest(URL: NSURL(string: encodedUrl)!)
        let session = NSURLSession.sharedSession()
        
        request.HTTPMethod = "GET"
        let task = session.dataTaskWithRequest(request, completionHandler: {data, response, error -> Void in
            if error == nil {
                var jsonResults : AnyObject?
                
                do {
                    jsonResults = try NSJSONSerialization.JSONObjectWithData(data!, options: [])
                } catch {
                    print("Fetch failed: \((error as! NSError).localizedDescription)")
                }
                
            }
            
        })
        
        task.resume()
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        registerSettingsAndCategories()
        return true
    }
    
    func application(application: UIApplication, handleActionWithIdentifier identifier: String?, forRemoteNotification userInfo: [NSObject : AnyObject], completionHandler: () -> Void) {
        if identifier == "phoneme"{
            if let url = NSURL(string: "tel://\("+9843056230")") {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

